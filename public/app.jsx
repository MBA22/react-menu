var LinkComponent = React.createClass({
    linkClick: function () {
        this.props.onClick(this.props.txt);
    },

    render: function () {
        var url = '/'
            + this.props.txt
                .toLowerCase()
                .trim()
                .replace(' ', '-');
        return (
            <span>
           <a onClick={this.linkClick} href={url}>{this.props.txt}</a>
        </span>);


    },
});
var MenuComponent = React.createClass({
    handelChange: function (text) {
        this.setState({
            heading: text,
        });
    },

    getInitialState: function () {
        return ({
            heading: 'Home',
        });
    },

    render: function () {
        var menuItems = ['Home',
            'Our Services',
            'About US',
            'Contact us'];
        return (
            <div>
                <nav className="navbar navbar-default">
                    <div class="container-fluid">
                        <ul class="nav navbar-nav">
                            { menuItems.map(function (item, idx) {
                                return (<span key={idx}><LinkComponent onClick={this.handelChange}
                                                                       txt={item}></LinkComponent></span>);
                            }.bind(this))
                            }
                        </ul>
                    </div>
                </nav>
                <h2>{this.state.heading}</h2>
            </div>
        );

    },
});


var Main = React.createClass({
    render: function () {
        return (

                <MenuComponent></MenuComponent>
        );
    },
});

ReactDOM.render(<Main></Main>, document.getElementById('app'));